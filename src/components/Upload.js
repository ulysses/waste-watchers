import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import axios from 'axios';

class Upload extends Component {

    state = {
        selectedFile: null
    }

    fileSelectedHandler = event => {
        console.log(event.target.files[0]);
        this.setState({ selectedFile: event.target.files[0]})
    }

    fileUploadHandler =() => {
        const fd = new FormData();
        fd.append('image', this.state.selectedFile, this.state.selectedFile.name)
        console.log('file is uploaded')
        //axios.post('my_endpoint');
    }

    render() {
        return (
            <div>
              <input type="file" onChange={this.fileSelectedHandler} />
              <Button onClick={this.fileUploadHandler}>Upload</Button>  
            </div>
        );
    }
}

export default Upload;