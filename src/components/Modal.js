import React, { Component } from "react";
import {
  Modal,
  Button,
  Form

} from "react-bootstrap";
import classes from './Modal.module.css'
import * as moment from "moment";
//import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import {isMobile} from 'react-device-detect';
//import Cam from './Cam';
import Cam from './camera/Cam';
import Upload from './Upload';
import CamModal from './CamModal';
//import axios from "./axios-subs";

//import images for poop diagram
import p1 from '../assets/p1.png';
import p2 from '../assets/p2.png';
import p3 from '../assets/p3.png';
import p4 from '../assets/p4.png';
import p5 from '../assets/p5.png';
import p6 from '../assets/p6.png';
import p7 from '../assets/p7.png';
import pullPaper from '../assets/pull_paper.png';
import collapse from '../assets/arrowCollapse.png';
import expand from '../assets/arrowExpand.png';
import camera from '../assets/camera.png';
import photos from '../assets/photos.png';

const Options = [...Array(100).keys()]

class Example extends Component {

  state = {
    isChecked: false,
    floor: 0,
    min: 5,
    diet: 'Mixed',
    shape: 'Solid',
    beer: 'no',
    showPoopDiag: false,
    poopDiagChoice: 0,
    cameraLoad: false,
    color: 'red',
    camColor: 'red',
    showCamModal: false,
    iconData: expand,
    camIconData: expand,
    dataUri:null
  };

  restState =() => {
    this.setState({
      isChecked: false,
      floor: 0,
      min: 5,
      diet: 'Mixed',
      shape: 'Solid',
      beer: 'no',
      showPoopDiag: false,
      poopDiagChoice: 0,
      cameraLoad: false,
      color: 'red',
      camColor: 'red',
      showCamModal: false,
      iconData: expand,
      camIconData: expand,
      dataUri:null
    })
  }

  toggleChange = () => {
    this.setState({
      isChecked: !this.state.isChecked,
    });
    if (this.state.isChecked === true) {
      this.setState({
        beer: 'no'
      });
    } else {
      this.setState({
        beer: 'yes'
      });
    }
  }


  selectFloor = (event) => {
    // console.log(event.target.value)

    this.setState({

      floor: event.target.value
    });

  }

  selectTime = (event) => {
    // console.log(event.target.value)

    this.setState({

      min: event.target.value
    });

  }

  selectDiet = (event) => {
    // console.log(event.target.value)

    this.setState({

      diet: event.target.value
    });

  }

  selectShape = (event) => {
    // console.log(event.target.value)

    this.setState({

      shape: event.target.value
    });

  }



  onSub = e => {
    const poop = this.props.data

    var fin = { lat: poop.lat, lon: poop.lon, poops: poop.poop, beer: this.state.beer, floor: this.state.floor, min: this.state.min, diet: this.state.diet, shape: this.state.shape }
    //console.log(fin)  
    fetch(
      `https://data-frontiers.info:4500/add?time=${moment().utcOffset('-0500').format("YYYY-MM-DD HH:mm:ss").substr(0, 18) + '0'
      }&lat=${fin.lat}&lon=${fin.lon}&floor=${fin.floor}&poops=${fin.poops}&beer=${fin.beer}&min=${fin.min}&diet=${fin.diet}&shape=${fin.shape}`,
      { method: "POST" }
    ).catch(err => console.error(err));


    this.props.onClose && this.props.onClose(e);

    //rest state
    this.restState();

  };

  onClose = e => {

    const poop = this.props.data

    var fin = { lat: poop.lat, lon: poop.lon, poops: poop.poop, beer: 'NA', floor: 0, min: 5, diet: 'NA', shape: 'NA' }
    //console.log(fin)  
    fetch(
      `https://data-frontiers.info:4500/add?time=${moment().utcOffset('-0500').format("YYYY-MM-DD HH:mm:ss").substr(0, 18) + '0'
      }&lat=${fin.lat}&lon=${fin.lon}&floor=${fin.floor}&poops=${fin.poops}&beer=${fin.beer}&min=${fin.min}&diet=${fin.diet}&shape=${fin.shape}`,
      { method: "POST" }
    ).catch(err => console.error(err));


    this.props.onClose && this.props.onClose(e);
    //this.props.addFlush()
    this.restState();
  };


  //these are meant for the poop diagram----------------------------------
  handlePoop = (e) => {
    console.log(e)
    this.setState({ showPoopDiag: false, shape: e })
  }


  handleShow = () => {
    this.setState({ showPoopDiag: true, color: "green" })
  }

  handleClose = () => {
    this.setState({ showPoopDiag: false })
  }
  //-----------------------------------------------------------------------


  //add record when toilet button is clicked
  //go here to see data: http://54.213.155.240:4000/products
  //ex: http://54.213.155.240:4000/add?time=2019-10-01%2009:00:00&lat=43.07&lon=-73.08&floor=1
  // addFlush = () => {

  //   fetch(
  //     `http://18.191.231.245:4500/add?time=${moment().format(
  //       "YYYY-MM-DD HH:mm:00"
  //     )}&lat=${this.state.lat}&lon=${this.state.lon}&floor=${this.state.floor}`,
  //     { method: "POST" }
  //   ).catch(err => console.error(err));
  // };

//photo taking
startCamera = () => {
  console.log('starting camera')
  if(!this.state.cameraLoad) {
    this.setState({ cameraLoad: true, camIconData: collapse}) ;  
  } else {
    this.setState({ cameraLoad: false, camIconData: expand}) ;
  }
  
} 

 onTakePhoto = (dataUri) => {
   // Do stuff with the dataUri photo...
   console.log('photo taken');
   this.setState({ camColor: 'green' })
 }


 onCameraError =(error) => {
   console.error('onCameraError', error);
   this.setState({ cameraLoad: false });
 }

 onCameraStart = (stream) => {
   console.log('onCameraStart');
 }

 onCameraStop = () => {
   console.log('onCameraStop');
 }

// modal control after photo is taken
uploadPhoto = e => {
  console.log('upload photo')
  this.setState({ camColor: 'green', dataUri: null, cameraLoad: false });
};

handleTakePhotoAnimationDone = (dataUri) => {
  console.log('takePhoto');
  this.setState({dataUri : dataUri});
  //props.onTakePhoto();
}

retakePhoto = () => {
  console.log('retake photo')
  this.setState({ camColor: 'red', dataUri: null, cameraLoad: true });
}

  render() {
    // if (!this.props.show) {
    //   return null;
    // }
     //console.log(this.state.cameraLoad)
    return (

      <Modal style={{ backgroundColor: '#3c303033' }} show={this.props.show} onHide={e => {
        this.onClose(e);
      }}>
        <Modal.Header closeButton>
          <Modal.Title style={{textAlign: "center", fontSize: "1.25em"}}>Enter Advanced Pooper Information</Modal.Title>
        </Modal.Header>
        <Modal.Body>

          <p>Enter more information about your session and take a toilet sheet to go. Don't be afraid, it's all anonymous and besides ... everyone poops!</p>

          <Form>


            <Form.Group controlId="formBasicPassword">
              <Form.Label>How long did your session take? (in mins)</Form.Label>
              <Form.Control as="select" onChange={event => this.selectTime(event)}>
                {Options.map((i) =>
                  <option>{i}</option>
                )}
              </Form.Control>
              <Form.Label>How would you describe your recent diet?</Form.Label>
              <Form.Control as="select" onChange={event => this.selectDiet(event)}>

                <option>Mostly Healthy</option>
                <option>Mostly Junk</option>
                <option>Mixed</option>

              </Form.Control>
              {/* <Form.Label>In what shape was your masterpiece?</Form.Label>
              <Form.Control as="select" onChange={event => this.selectShape(event)}>

                <option>Solid</option>
                <option>Mush</option>
                <option>Liquid</option>

              </Form.Control> */}
              <Form.Label>What floor is your bathroom on?</Form.Label>
              <Form.Control as="select" onChange={event => this.selectFloor(event)}>
                {Options.map((i) =>
                  <option>{i}</option>
                )}
              </Form.Control>
            </Form.Group>
            <fieldset>

              <Form.Group controlId="choosePoopPic">
                <p>Select the image which best describes your poop:</p>
                <Button variant="secondary" onClick={this.handleShow} style={{backgroundColor : this.state.color}}>
                <img src={photos} />
                <img src ={this.state.iconData} />
                </Button>
              </Form.Group>

               <Modal show={this.state.showPoopDiag} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title style={{textAlign:"center",fontSize:"1.25em"}}>Which best describes your poop?</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                  <div className={classes.container}>
                    <p>Click on the best option: </p>
                    <img className={classes.images} src={p1} value={1} alt="p1" onClick={this.handlePoop.bind(this, '1')} />
                    <br />
                    <img className={classes.images} src={p2} value={2} alt="p2" onClick={this.handlePoop.bind(this, '2')} />
                    <br />
                    <img className={classes.images} src={p3} value={3} alt="p3" onClick={this.handlePoop.bind(this, '3')} />
                    <br />
                    <img className={classes.images} src={p4} value={4} alt="p4" onClick={this.handlePoop.bind(this, '4')} />
                    <br />
                    <img className={classes.images} src={p5} value={5} alt="p5" onClick={this.handlePoop.bind(this, '5')} />
                    <br />
                    <img className={classes.images} src={p6} value={6} alt="p6" onClick={this.handlePoop.bind(this, '6')} />
                    <br />
                    <img className={classes.images} src={p7} value={7} alt="p7" onClick={this.handlePoop.bind(this, '7')} />
                  </div>


                </Modal.Body>
             
              </Modal>
                  { isMobile ?
                  <div>
                    <p>Take a photo of your duty so we can study it: </p>
                  <Button
                    onClick={this.startCamera}
                    style={{backgroundColor : this.state.camColor}}
                  >
                    <img src={camera} />
                    <img src ={this.state.camIconData} />
                  </Button>
                  <br />
                  <br />
                  </div> : <div></div>
                  }
                  {this.state.cameraLoad ? 
                    <Cam 
                      cameraLoad={this.state.cameraLoad}
                      startCamera = {this.startCamera}
                      dataUri={this.state.dataUri}
                      onTakePhoto={this.onTakePhoto}
                      retakePhoto={this.retakePhoto}
                      onCameraError={this.onCameraError}
                      onCameraStart={this.onCameraStart}
                      onCameraStop={this.onCameraStop}
                      uploadPhoto={this.uploadPhoto}
                      handleTakePhotoAnimationDone ={this.handleTakePhotoAnimationDone}
                    /> : <div></div>}
              <Form.Group>
                <Form.Label as="legend">
                  Have you drank Beer in the last 12 hours?
                </Form.Label>

                <Form.Check
                  required
                  label="YES"
                  name="yes"
                  id="rad1"
                  checked={this.state.isChecked}
                  onChange={this.toggleChange}
                />
              </Form.Group>
            </fieldset>
          </Form>



           <Button
            style={{
              border: "none !important",
            
              backgroundPosition: "center center",
              width: "80px",
              height: "80px",
              margin: "5% 42%",
              borderRadius: "50%",
              fontSize: "15px",
              backgroundImage: `url(${pullPaper})`,
              backgroundSize: "120% 120%",
              border: "none",
              cursor:"pointer",
              overflow: "hidden",
              outline:"none"
            }} onClick={e => { this.onSub(e) }}></Button>
        </Modal.Body>


      </Modal>
      //   <Container>


      //     <Modal
      //       isOpen={this.props.show}
      //       style={{ backgroundColor: "yellow" }}
      //     >

      //       <ModalBody>
      //         This app allows you to track the international space station in real
      //         time. Click on 'ISS information' to see the coordinates and some
      //         other stuff. Leave it for a while and you will see the breadcrumbs
      //         trail of the ISS.
      //       </ModalBody>
      //     </Modal>

      //      <Button variant="secondary" onClick={this.handleClose}>
      //                  CLose
      //              </Button>

      //   </Container>

      // 

    );
  }
}

export default Example;


/**
 * <Form.Group controlId="choosePoopPic">
                <Form.Label>Select the choice which best describes your poop:</Form.Label>
                <Form.Control as="select" onClick={this.handleShow}>
                  <option>{this.state.poopDiagChoice}</option>
                </Form.Control>
              </Form.Group>

              <Modal show={this.state.showPoopDiag} onHide={this.handleClose}>
                <Modal.Header closeButton>
                  <Modal.Title>Which best describes your poop?</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                  <div className={classes.container}>
                    <p>Click on the best option: </p>
                    <img className={classes.images} src={p1} value={1} alt="p1" onClick={this.handlePoop.bind(this, 1)} />
                    <br />
                    <img className={classes.images} src={p2} value={2} alt="p2" onClick={this.handlePoop.bind(this, 2)} />
                    <br />
                    <img className={classes.images} src={p3} value={3} alt="p3" onClick={this.handlePoop.bind(this, 3)} />
                    <br />
                    <img className={classes.images} src={p4} value={4} alt="p4" onClick={this.handlePoop.bind(this, 4)} />
                    <br />
                    <img className={classes.images} src={p5} value={5} alt="p5" onClick={this.handlePoop.bind(this, 5)} />
                    <br />
                    <img className={classes.images} src={p6} value={6} alt="p6" onClick={this.handlePoop.bind(this, 6)} />
                    <br />
                    <img className={classes.images} src={p7} value={7} alt="p7" onClick={this.handlePoop.bind(this, 7)} />
                  </div>


                </Modal.Body>
                <Modal.Footer>
                  
                </Modal.Footer>
              </Modal>


               cam = null;
 renderCamera =() => {
  if(isMobile) {
     this.cam =(
        <Cam 
          cameraLoad={this.state.cameraLoad}
          startCamera = {this.startCamera}
          onTakePhoto={this.onTakePhoto}
          onCameraError={this.onCameraError}
          onCameraStart={this.onCameraStart}
          onCameraStop={this.onCameraStop}
          camColor = {this.state.camColor}
          />
     )
     return this.cam;
   }
 }
 * 
 */