import axios from "axios";

const instance = axios.create({
    baseURL: 'https://hamburgesa-51dc3.firebaseio.com/'
});

export default instance;