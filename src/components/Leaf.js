import React, { Component } from 'react';
//import { render } from 'react-dom';
import { Map, TileLayer } from 'react-leaflet';
import HeatmapLayer from 'react-leaflet-heatmap-layer';
//import axios from 'axios';
//import StatCard from './smallComponents/StatCard';
import classes from './leaf.module.css';
//import { Container } from 'react-bootstrap';
//import { addressPoints } from './realworld.10000.js';

class Leaf extends Component {



  render() {
    //hook up to state for data 


  
    //console.log(this.props.newHeatMapDataPoints);
    console.log(this.props.newHeatMapDataPoints.length);
    //console.log('testing props pass into leaf',this.props.heatmapData)
    
    let cleanData = [];
    this.props.newHeatMapDataPoints.map(data=>{
      cleanData.push(Object.values(data));
    }) 

    const showData=()=>{
      console.log(cleanData.length)
    }
   
    return (

      <div>
      <Map center={[40.7484, -73.9857]}
        zoom={12}
        zoomControl= {false}
        //style= {mapStyles}
        className={classes.map}
      >
      {
      // cleanData.length > 0 ?
      <HeatmapLayer
            fitBoundsOnLoad
            // fitBoundsOnUpdate
            points={cleanData} //input state data here i.e. addressPoints
            
            longitudeExtractor={m => m[1]}
            latitudeExtractor={m => m[0]}
            intensityExtractor={m => parseFloat(m[2])}
            max = {4000}
            radius = {8}
            blur = {18}
            />
      //  : <div></div>
      }
            <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url= 'http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png'
          // "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />


      </Map>

      </div>
    );
  }

}

export default Leaf;


/**
 * old code
 * 
    let test = [
                    [40.7484,-73.9857, 486],
                    [40.7516, -73.9755, 807]
                ]



                ,
      [40.7489, -73.9680, "899"],
      [40.7359, -73.9911, "1273"],
      [40.7397, -73.8408, "1258"],
      [40.7644, -73.9235, "1279"]


          constructor(props) {
      super(props)
      this.state = {
        liveCount : null
      }
    } 



    //for now call database directly for data (move to app state probably) 
     componentDidMount() {
        this.fetchData();
        console.log(this.state.liveCount);
    }

    fetchData=()=>{
      const getStats = async () => {
        await fetch('http://54.213.155.240:4000/stats')
          .then(res => res.json())
          .then(res =>
            //console.log(res.data[0].poops)
            this.setState({ liveCount: res.data[0].poops })
            )
      }
  
      getStats();
    }


 * 
 */