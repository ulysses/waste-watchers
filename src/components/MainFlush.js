import React, { Component } from 'react';
//import ReactDOM from 'react-dom';
import { Button}  from 'react-bootstrap';
import flush from '../assets/flushButton.png';
import classes from './mainFlush.module.css';

class MainFlush extends Component {
    render() {
        const btnStyle={
            borderRadius: "50%",
            backgroundColor: "blue"
        }
        return (
            <Button variant="contained" color="primary">
                <img className={classes.image} src={flush} alt='btn' />
            </Button>
        );
    }
}

export default MainFlush;