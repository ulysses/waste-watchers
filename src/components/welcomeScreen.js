import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import classes from './welcomescreen.module.css';
import flush from '../assets/flush.png';
import flies from '../assets/flies.jpg';

class Welcome extends Component {
  state = {
    show: null
  };

  componentDidMount() {
    if (sessionStorage.getItem("show") === null) {
      this.setState({ show: true });
    } else this.setState({ show: sessionStorage.getItem("show") == true });
  }

  Click = e => {
    this.setState({ show: false });
    sessionStorage.setItem("show", false);
  
  };

  render() {
    
    return (
      <Modal show={this.state.show} backdrop={true} 
        //dialogClassName={"modal-dialog modal-content"}
        dialogClassName={classes.modal}
          
          >
        <Modal.Header >
          <Modal.Title>Welcome to Pooper!</Modal.Title>
        </Modal.Header>
        <Modal.Body >
          Never again you will suffer the pain of going number two and not
          telling the world about it! Click on the 'Ceramic Throne' in the
          middle to report your magnificent deed!
          
          <Button
            style={{
              border: "none !important",
              backgroundColor: "Transparent",
              backgroundPosition: "center center",
              width: "80px",
              height: "80px",
              margin: "5% 42%",
              borderRadius: "50%",
              fontSize: "15px",
              backgroundImage: `url(${flush})`,
              backgroundSize: "130% 130%",
              border: "none",
              cursor:"pointer",
              overflow: "hidden",
              outline:"none"
            }}
            onClick={e => {
              this.Click(e);
            }}
            className ={classes.btn}
          >
         
          </Button>
        
        </Modal.Body>
      </Modal>
    );
  }
}

export default Welcome;


/**
 *   <img src ={flush} alt="logo button" />
 * 
 * 
 * 
 * questions to ask
 * 
 * 
 * Which of these best describes your diet?
 * healthy
 * junk food
 * mixed
 * 
 * 
 * 
 * What is the consistency of your deuce?
 * morgan freeman ,trump, stallone (dropdown, liquid, mush, hard)
 * 
 * 
 * 
 * how long did it take to do your duty?
 * 
 * 
 * 
 * 
 */