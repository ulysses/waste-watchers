import React, { Component } from 'react';
import {
    Button
  } from "react-bootstrap";
import Camera, {FACING_MODES} from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';

class Cam extends Component {

    state = {
        iconData: ['https://img.icons8.com/material/24/000000/expand-arrow--v1.png',
                   'https://img.icons8.com/material/26/000000/collapse-arrow--v1.png'
                  ],
        arrIcon : 'https://img.icons8.com/material/24/000000/expand-arrow--v1.png'
    }

    //toggleArrow =() => {
    //    this.setState({ arrIcon: iconData[1] })
    //}

    render() {
        return (
            <div> 
                <Button 
                    onClick = {this.props.startCamera}
                    style={{backgroundColor : this.props.camColor}}
                    >
                        Take a Photo of your Duty/Upload a Photo
                        <img src ={this.state.arrIcon} />
                </Button>
                    { this.props.cameraLoad ?
                        <Camera
                            isFullscreen = {false}
                            idealFacingMode = {FACING_MODES.ENVIRONMENT}
                            onTakePhoto = { (dataUri) => { this.props.onTakePhoto(dataUri); } }
                            onCameraError = { (error) => { this.props.onCameraError(error); } }
                            onCameraStart = { (stream) => { this.props.onCameraStart(stream); } }
                            onCameraStop = { () => { this.props.onCameraStop(); } }
                        /> : <div></div>
                    }
            </div>
        );
    }
}

export default Cam;