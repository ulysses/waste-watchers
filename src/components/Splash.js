import React, { Component } from 'react';
import toilet from "../assets/toilet.png";
import classes from './splashScreen.module.css';

class Splash extends Component {
    render() {
        return (
            <div className={classes.splash}>
                <img src={toilet}/>
                <div className={classes.iconText}>WasteWatchers... </div>
            </div>
        );
    }
}

export default Splash;