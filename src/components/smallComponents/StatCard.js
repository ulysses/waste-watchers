import React from 'react';
import { Card } from 'react-bootstrap';
import classes from './statcard.module.css';

const StatCard = (props) => {

    return (
        <Card className={classes.cards}>
            <Card.Body className={classes.body}>
                <h4 className={classes.title}>{`${props.text}`}</h4>
            </Card.Body>
        </Card>
    )
};

export default StatCard;