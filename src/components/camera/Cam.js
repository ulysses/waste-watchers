import React, { Component } from 'react';
import Camera, {FACING_MODES} from 'react-html5-camera-photo';
import { Button } from 'react-bootstrap';
import ImagePreview from './ImagePreview';
import classes from './cam.module.css';

class Cam extends Component {

  render() {
    return (
      <div>
      {
        (this.props.dataUri)
          ? <div><ImagePreview dataUri={this.props.dataUri}
            //isFullscreen={isFullscreen}
          />
          <div className={classes.btn}>
            <Button onClick={this.props.uploadPhoto} style={{width: '45%'}}>
               Upload Photo 
            </Button>
            <Button onClick={this.props.retakePhoto} style={{width: '45%'}}>
               Retake Photo
            </Button>
          </div>
          </div>
          : <Camera 
              idealFacingMode = {FACING_MODES.ENVIRONMENT}
              onTakePhotoAnimationDone = {this.props.handleTakePhotoAnimationDone}
            //isFullscreen={isFullscreen}
          />
        }
      </div>
    );
  }
}

export default Cam;



