import React from 'react';
import PropTypes from 'prop-types';
import classes from './imagepreview.module.css';
export const ImagePreview = ({ dataUri, isFullscreen }) => {
  let classNameFullscreen = isFullscreen ? 'demo-image-preview-fullscreen' : '';

  return (
    <div className={classes.preview}>
      <img src={dataUri} className={classes.image} />
    </div>
  );
};

//ImagePreview.propTypes = {
//  dataUri: PropTypes.string,
//  isFullscreen: PropTypes.bool
//};

export default ImagePreview;