import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';

class CamModal extends Component {

    render() {
        return (
            <Modal show={this.props.showCamModal}  
              >
            <Modal.Header >
              <Modal.Title>Is This Picture Okay?</Modal.Title>
            </Modal.Header>
            <Modal.Body >
              Would you like to send this photo or retake it?
              <Button
                onClick={e => {this.props.UploadPhoto(e)}}
              >
                Upload this Photo
              </Button>
              <Button
                onClick={e => {this.props.UploadPhoto(e)}}
              >
                Retake Photo
              </Button>
            
            </Modal.Body>
          </Modal>
        );
    }
}

export default CamModal;