import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';

class AdvancedFlush extends Component {
    render() {
        return (
            <Button variant="contained" color="primary">
                Advanced Flush
            </Button>
        );
    }
}

export default AdvancedFlush;