import React, { Component } from 'react';
import Leaf from './Leaf';
import Navigator from './Navigator';
import StatCard from './smallComponents/StatCard';

import classes from './dash.module.css';
import Graph from './smallComponents/Graph';
import {
  LineChart, Line,
  XAxis, YAxis,
  CartesianGrid, Tooltip,
  BarChart, Bar,
  
} from 'recharts';
//import Countdown from 'react-countdown-now';
import * as moment from "moment";

class Dash extends Component {

  constructor(props) {
    super(props)
    this.state = {
      messages: ["Calculating poop...", "Cleaning your toilet...", "Calling your doctor...", "Unglogging the toilet...", "Dropping the deuce..."],
      loading:true,
      location: null,
      totalPoops:null,
      hourPoops:null,
      windowPoopsTenSecs:null,
      newHeatMapDataPoints:[],
      dayPoops:[],
      hourlyPoops:[],
      timestamp: moment().format("YYYY-MM-DD HH:mm:00"),
      windowStart: moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:00"),
      windowStartDay: moment().subtract(24, "hours").format("YYYY-MM-DD HH:mm:00")
    }
  }


  componentDidMount () {

    // call 10 second window every 10 seconds
    setInterval(async () => {
      //fetch window 
      this.fetchWindow();
      
      //add 10 secs to hour
      if(this.state.windowPoopsTenSecs) { this.updateHourlyPoops(); this.uppdateTotalPoops(); };
      
      },15000)

    // call the rest only once
    this.fetchHour();
    this.fetchSum();
    this.fetchDailies();
    this.fetchHourlies();
    
  }

  updateHourlyPoops =() => {
    let updatedHourlyPoops = this.state.hourPoops + this.state.windowPoopsTenSecs;
    this.setState({ hourPoops : updatedHourlyPoops });
  }

  uppdateTotalPoops = () => {
    let updatedTotalPoops = this.state.totalPoops + this.state.windowPoopsTenSecs;
    this.setState({ totalPoops : updatedTotalPoops });
  }

//fetching data functions---------------------------------------------------------

//ten second window
fetchWindow = async () => {
      await fetch('https://data-frontiers.info:4500/stats')
        .then(res => res.json())
        .then(res => {
          let resFilter = res.map( o => { return { lat: o.lat, lon: o.lon, time: o.time} })
          this.setState({ windowPoopsTenSecs: resFilter.length })
        }
        )
    }

//one hour window
fetchHour = async () => {
    let end = moment().format("YYYY-MM-DD HH:mm:00");
    let start = moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:00");

     await fetch(`https://data-frontiers.info:4500/poops/${start}/${end}`)
       .then(res => res.json())
       .then(res => {

        //produce points for map
        let resFilter = res.data.map( o => { return { lat: o.lat, lon: o.lon, time: o.time} });

        //total poops in this hour window
        let hourPoops = resFilter.length;
    
         this.setState({ hourPoops: hourPoops, newHeatMapDataPoints: resFilter })
       }
       )
   }


//total poops 
fetchSum = async () => {
  await fetch('https://data-frontiers.info:4500/sum')
  .then(res => res.json())
  .then(res => {
    let allPoops=res[0].poops;
    this.setState({ totalPoops: allPoops })
  }
  )
}

//last 30 days for graph 1
fetchDailies = async () => {
  //let end = moment().format("YYYY-MM-DD HH:mm:00");
  //let start = moment().subtract(30, "days").format("YYYY-MM-DD HH:mm:00");
  await fetch(`https://data-frontiers.info:4500/ppday`)
  .then(res => res.json())
  .then(res => {
    console.log(res)
    this.setState({ dayPoops: res})
  }
  )
}

//last 24 hours for graph 2
fetchHourlies = async () => {
  //let end = moment().format("YYYY-MM-DD HH:mm:00");
  //let start = moment().subtract(24, "hours").format("YYYY-MM-DD HH:mm:00");
  await fetch(`https://data-frontiers.info:4500/pphour`)
  .then(res => res.json())
  .then(res => {
    //filter hour only hour and poops
    //let resFilter = res.data.map( o => { return { hour: o.hour, poops: o.poops} })
    this.setState({ hourlyPoops: res})
  }
  )
}

  render() {


    //console.log(this.props.newHeatMapDataPoints)
    return (
      <div>
        <Navigator />
        <div className={classes.container}>
          <StatCard text={this.state.totalPoops ? `All Poops Forever and Ever: ${this.state.totalPoops.toLocaleString(this.state.totalPoops,{ minimumFractionDigits: 0 })}` : this.state.messages[0]} />
          <StatCard text={this.state.windowPoopsTenSecs ? `Poops in last 10 Seconds: ${this.state.windowPoopsTenSecs}` : this.state.messages[1]} />
          <StatCard text={this.state.hourPoops ? `All Poops in Last Hour: ${this.state.hourPoops.toLocaleString(this.state.hourPoops,{ minimumFractionDigits: 0 })}` : this.state.messages[2]} />
        </div>
        <div style={{ backgroundColor: '#343a40' }}>
          
          <Leaf
            newHeatMapDataPoints={this.state.newHeatMapDataPoints}
          />
          <div className={classes.graphsContainer}>
          { this.state.dayPoops.length > 0 ? 
            <Graph style={{ magrin: '5px auto' }}
              title={'Poops per Day'}
              graph={
                <BarChart
                  width={500}
                  height={300}
                  data={this.state.dayPoops}
                  margin={{ top: 5, right: 40, left: 10, bottom: 5 }}>
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="day" angle={0} textAnchor="end" tick={{ fontSize: 13, fill: "white" }} />
                  <YAxis tick={{ fill: 'white' }}/>
                  <Tooltip />
                  
                  <Bar dataKey="poops" fill="#8884d8" />
                </BarChart>}
            /> :  <StatCard text={this.state.messages[3]}  />
            }
            { this.state.hourlyPoops.length > 0 ? 
            <Graph
              title={'Poops per Hour in Last 24 Hours'}
              graph=
              {<LineChart
                data={this.state.hourlyPoops}
                margin={{ top: 5, right: 40, left: 10, bottom: 5 }}>
                <CartesianGrid strokeDasharray="4 4" />
                <XAxis dataKey="hour" angle={0} textAnchor="end" tick={{ fontSize: 13, fill: "white" }} />
                <YAxis tick={{ fill: 'white' }} />
                <Tooltip />
                
                <Line type="monotone" dataKey="poops" stroke="white" activeDot={{ r: 8 }} />
              </LineChart>}
            /> : <StatCard text={this.state.messages[4]} />}
          </div>
        </div>
        <div style={{ backgroundColor: "#363a42"}}>
          <p
            style={{
              width: "90%",
            
              margin: "auto 5%",
              padding: "30px",
              color: "white",
              textAlign: "center"
            }}
          >
            2019. Property of VeryCool-Studio. All Rights Reserved.
          </p>
        </div>
      </div>
    );
  }
}

export default Dash;



/**
 * old code
 * 
 * 
 * 
 * counter
 * <div className={classes.counter}>
            <Countdown date={Date.now() + 10000} />
          </div>
 * 
 *  <Container fluid style={{ paddingLeft: 0, paddingRight: 0 }}>
        <Navigator />

        <div>
          <div className={classes.container}>
          <StatCard text={`Total Poops: `} liveCount={this.state.poops ? this.state.poops : LoadingMessage()} />
          <StatCard text={`Total Live Poops: `} liveCount={this.state.poops ? this.state.poops : this.state.loading}/>
          <StatCard text={`Total Poops Today: `} liveCount={this.state.poops ? this.state.poops : this.state.loading} />
          </div>
        </div>
        <Leaf
          heatmapData={this.props.heatmapData}
          heatmapDataTest={this.props.heatmapDataTest}
        />
        <div className={classes.graphsContainer}>
          <Graph
            title={'Types of Poops per Day'}
            graph={
              <AreaChart
                width={500}
                height={400}
                data={data01}
                margin={{
                  top: 10, right: 30, left: 0, bottom: 0,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="day" angle={0} tick={{ fontSize: 13, fill: "white" }}  />
                <YAxis tick={{ fill: 'white' }}  />
                <Tooltip />
                <Area type="monotone" dataKey="uv" stackId="1" stroke="#8884d8" fill="#8884d8" />
                <Area type="monotone" dataKey="pv" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
              </AreaChart>
            }
          />

          <Graph
            title={'Total Poops per Day'}
            graph=
            {<LineChart
              data={data02}
              margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
              <CartesianGrid strokeDasharray="4 4" />
              <XAxis dataKey="day" angle={0} textAnchor="end" tick={{ fontSize: 13, fill: "white" }} />
              <YAxis tick={{ fill: 'white' }} />
              <Tooltip />
              <Legend />
              <Line type="monotone" dataKey="value" stroke="white" activeDot={{ r: 8 }} />
            </LineChart>}
          />
        </div>
      </Container>


        //for now call database directly for data (move to app state probably) 
  componentDidMount() {
    //this.fetchData();
    //console.log(this.state.liveCount);

    //temorarily call totals from dash
    //this.fetchAwsData();

    //fetch firebase data
  }

  fetchData = () => {
    const getStats = async () => {
      await fetch('http://18.191.231.245:4500/stats')
        .then(res => res.json())
        .then(res =>
          //console.log(res.data[0].poops)
          this.setState({ dataPoints: res.data })
        )
    }

    getStats();
  }


        liveCount: null,
      dataPoints: [],
      data: {
        poop: 1,
        timestamp: Date.now(),
        dataPoints: []
      },




      fetchWindow = async () => {
      await fetch('https://data-frontiers.info:4500/stats')
        .then(res => res.json())
        .then(res => {
          let resFilter = res.map( o => { return { lat: o.lat, lon: o.lon, time: o.time} })
          this.setState({ windowPoopsTenSecs: resFilter.length })
        }
        )
    }

//one hour window
fetchHour = async () => {
    let end = moment().format("YYYY-MM-DD HH:mm:00");
    let start = moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:00");

     await fetch(`https://data-frontiers.info:4500/poops/${start}/${end}`)
       .then(res => res.json())
       .then(res => {

        //produce points for map
        let resFilter = res.data.map( o => { return { lat: o.lat, lon: o.lon, time: o.time} });

        //total poops in this hour window
        let hourPoops = resFilter.length;
    
         this.setState({ hourPoops: hourPoops, newHeatMapDataPoints: resFilter })
       }
       )
   }


//total poops 
fetchSum = async () => {
  await fetch('http://data-frontiers.info:4500/sum')
  .then(res => res.json())
  .then(res => {
    let allPoops=res[0].poops;
    this.setState({ totalPoops: allPoops })
  }
  )
}

//last 30 days for graph 1
fetchDailies = async () => {
  let end = moment().format("YYYY-MM-DD HH:mm:00");
  let start = moment().subtract(30, "days").format("YYYY-MM-DD HH:mm:00");
  await fetch(`http://data-frontiers.info:4500/ppday/${start}/${end}`)
  .then(res => res.json())
  .then(res => {
    console.log(res.data)
    this.setState({ dayPoops: res.data})
  }
  )
}

//last 24 hours for graph 2
fetchHourlies = async () => {
  let end = moment().format("YYYY-MM-DD HH:mm:00");
  let start = moment().subtract(24, "hours").format("YYYY-MM-DD HH:mm:00");
  await fetch(`http://data-frontiers.info:4500/pphour/${start}/${end}`)
  .then(res => res.json())
  .then(res => {
    //filter hour only hour and poops
    let resFilter = res.data.map( o => { return { hour: o.hour, poops: o.poops} })
    this.setState({ hourlyPoops: resFilter})
  }
  )
}

 */