import React, { Component } from "react";

//import MainFlush from "./MainFlush";
import {

  Image
} from "react-bootstrap";
import classes from "./home.module.css";
import Navigator from "./Navigator";
import Example from "./Modal";
import toilet1 from "../assets/toilet1.png";
import Welcome from "./welcomeScreen";
//import * as d3 from "d3";
import * as moment from "moment";
import UIfx from 'uifx';

//bring in images of rug to cycle through
import rug from "../assets/rugFrames/rugC.png";
import rug1 from "../assets/rugFrames/rugC1.png";
import rug2 from "../assets/rugFrames/rugC2.png";
import rug3 from "../assets/rugFrames/rugC3.png";
import rug4 from "../assets/rugFrames/rugC4.png";
import rug5 from "../assets/rugFrames/rugC5.png";
import rug6 from "../assets/rugFrames/rugC6.png";
import rug7 from "../assets/rugFrames/rugC7.png";
//import song from "../assets/toiletFlush.mp3";

//import { Container } from 'react-bootstrap';
//import Referral from './Referral';

var stamp = moment().format("YYYY/MM/DD HH:mm:00");
console.log(stamp);





class Home extends Component {
  state = {
    allPoops: "Calculating Poop...", //2343558 + Math.floor(Math.random() * 10 + 1),
    poops: "Calculating Poop...", //558 + Math.floor(Math.random() * 10 + 1),
    show: false,
    lat: 43.0,
    lon: -73.1,
    floor: 0,
    data: {
      poop: 1,
      timestamp: moment().utcOffset('-0500').format("YYYY-MM-DD HH:mm:ss").substr(0,18)+'0',
      lat: 43.0,
      lon: -73.1,
      
    },
    rugs:[rug,rug1,rug2,rug3,rug4,rug5,rug6,rug7],
    activeImageIndex: 0
  };


  fetchStats = async () => {
    fetch('https://data-frontiers.info:4500/stats')
   .then(res => res.json())
   .then(res => {
   
     let poopsSum = res.reduce(function(sum, d) {
       return sum + d.poops;
     }, 0);
     this.setState({ poops: poopsSum, allPoops: (this.state.allPoops + poopsSum)});
  });
  }


  componentDidMount() {

    setInterval(()=>{
        let newActiveIndex = this.state.activeImageIndex===7 ? 7 : this.state.activeImageIndex+1     
        this.setState({
          activeImageIndex: newActiveIndex
        })
    }, 900);
 

    this.fetchStats()

    setInterval(()=>{
      this.fetchStats()}, 15000);


    fetch("https://data-frontiers.info:4500/sum")
    .then(res => res.json())
    .then(res => {
      let allPoops = res[0].poops

      this.setState({ allPoops: allPoops });
  });
    
          }
      
      
      
  //     async () => {
  //     Promise.all([
  //       fetch("http://18.191.231.245:4500/stats"),
  //       fetch("http://18.191.231.245:4500/sum")
  //     ])
  //       .then(([res1, res2]) => Promise.all([res1.json(), res2.json()]))
  //       .then(([res1, res2]) => {
  //         // console.log(res2.data[0].poops)
  //         let poopsSum = res1.data.reduce(function(sum, d) {
  //           return sum + d.poops;
  //         }, 0);

  //         let allPoops = res2.data[0].poops.toLocaleString(res2.data[0].poops, {
  //           minimumFractionDigits: 0
  //         });

  //         this.setState({ poops: poopsSum, allPoops: allPoops });
  //       });
  //   }, 10000);
  // }

  seth() {
    navigator.geolocation.getCurrentPosition(position => {
      let setUserLocation = [
        position.coords.latitude,
        position.coords.longitude
        
      ];
      
      console.log(setUserLocation)
      this.setState({
        data: { lat: Number(setUserLocation[0].toFixed(3)) , lon: Number(setUserLocation[1].toFixed(3)) ,poop: 1, timestamp: moment().utcOffset('-0500').format("YYYY-MM-DD HH:mm:ss").substr(0,18)+'0' },
        show: !this.state.show
      });

      //set user state here
    });

 
  }

  showModal = e => {
    this.seth()
    console.log(this.state.show)
  };

  showM = e => {
    sessionStorage.setItem("showNav", true);
  };

  cycleRugs =()=> {
    let finalRug = this.state.rugs[0]; 
    //return this.state.rugs[0];
    this.state.rugs.map(e=>{
      finalRug = e;
    })
    return finalRug;
  }
  //test image load
  //<Image className={classes.rug} src={this.state.rugs[this.state.activeImageIndex]}  />
  //<Image className={classes.rug} src={rug}  />


  componentWillUnmount() {
    clearInterval(this.interval);
  }



  render() {

      //setup audio
      //const audio = new UIfx(song)

    return (
      <div>
        <div style={{ backgroundColor: "#9ea4bd",minHeight: '95vh'}}>
          <Navigator show={this.state.showNav} />
          <Welcome />
          <Example
            onClose={this.showModal}
            show={this.state.show}
            data={this.state.data}
            coords={this.state.coords}
            addFlush={this.addFlush}
          />

          <div className={classes.container}>
            <h3>Last Minute Flushes:</h3>

            <h3>Total Poops:</h3>
          </div>
          <div className={classes.container}>
            <h4>{this.state.poops}</h4>

            <h4>{this.state.allPoops.toLocaleString(
       
      )}</h4>
          </div>

          <Image
            className={classes.button}
            src={toilet1}
            onClick={e => {
              this.showModal();
              this.showM();
              //audio.play();
              console.log('click')
            }}
          />
          

          
          <Image className={classes.rug} src={this.state.rugs[this.state.activeImageIndex]}  />
        

          {/* <div
          className={classes.container}
          
        >
         

          <h5>By VeryCool-Studio!</h5>
        </div> */}
        </div>
        <div style={{ backgroundColor: "#363a42"}}>
          <p
            style={{
              width: "90%",

              margin: "auto 5%",
              padding: "30px",
              color: "white",
              textAlign: "center"
            }}
          >
            2019. Property of VeryCool-Studio. All Rights Reserved.
          </p>
        </div>
      </div>
    );
  }
}

export default Home;

/**
 * old code
 * 
 *  constructor(props) {
        super(props);
        this.state = {
            loading: true,
             location: null
        }
    }

    componentDidMount() {
        //mimic loading page
        setTimeout(() => {
            console.log('loading page')
            this.setState({
                loading: false
            });
        }, 2500)
    }

 */

