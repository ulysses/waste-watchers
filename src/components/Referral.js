import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';

class Referral extends Component {
    render() {
        return (
            <Button 
                variant="contained"
                color="primary" 
                style ={{marginLeft: "30%"}}>
                Refer a Friend
            </Button>
        );
    }
}

export default Referral;