import React, { Component } from 'react';
import {Form,Button} from 'react-bootstrap'
import classes from './contact.module.css'
import Navigator from "./Navigator";
import * as emailjs from 'emailjs-com';


class Contact extends Component {
    state = {
      email: '',
      message: '',
      subject: '',
      show: false
    }
    
    handleSubmit(e) {
  
      alert("Email is sent");
  
      e.preventDefault()    
      
      const { email, message, subject } = this.state    
      
      let templateParams = {
        from_name: email,
        to_name: 'babelofdata@gmail.com',
        subject: subject,
        message_html: message,
       }     
       
       emailjs.send(
        'gmail',
        'template_KxZ6nMx5',
         templateParams,
        'user_PsBDguKpQFEu7NJ2R82hM'
       )     
       this.resetForm()
   }
  
   
   resetForm() {
      this.setState({
        email: '',
        subject: '',
        message: '',
      })
    }
    
    handleChange = (param, e) => {
      this.setState({ [param]: e.target.value })
    }
   
      render(){
          return(
              <div>
  <div style = {{backgroundColor: '#9ea4bd',minHeight: '98vh'}}>
  <Navigator/>
  

  
  <h3 className={classes.titles}>Contact Form</h3> 
  
  
  <Form   onSubmit={this.handleSubmit.bind(this)}
         className = {classes.form}>
    <Form.Group controlId="exampleForm.ControlInput1">
      <Form.Label>Your Email address</Form.Label>
      <Form.Control 
        type="email" 
        placeholder="name@example.com" 
        value={this.state.email}
        onChange={this.handleChange.bind(this, 'email')} 
        />
    </Form.Group>
    <Form.Group controlId="exampleForm.ControlInput2">
      <Form.Label>Subject</Form.Label>
      <Form.Control 
        type="subject" 
        placeholder="My Subject" 
        value={this.state.subject}
        onChange={this.handleChange.bind(this, 'subject')} 
        />
    </Form.Group>
  
    <Form.Group controlId="exampleForm.ControlTextarea1">
      <Form.Label>Enter your Message</Form.Label>
      <Form.Control as="textarea" rows="10" 
          value={this.state.message}
          onChange={this.handleChange.bind(this, 'message')}
          placeholder="Contact Us."
          />
    </Form.Group>
    
    <Button variant="warning" type="submit" style = {{width: '38%',margin:'5% 31%', padding: '10px 20px'}}>
      Submit
    </Button>
  </Form>
  

          </div>

<div style={{ backgroundColor: "#363a42"}}>
<p
  style={{
    width: "90%",
  
    margin: "auto 5%",
    padding: "30px",
    color: "white",
    textAlign: "center"
  }}
>
2019. Property of VeryCool-Studio. All Rights Reserved.
</p>
</div>
</div>

          )
      }
  }
  
  export default Contact;