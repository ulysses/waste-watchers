import React, { Component } from 'react';
import Home from './components/Home';
import Dash from './components/Dash';
//import Settings from './components/Settings';
import Contact from './components/Contact';
import About from './components/About';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
//import axios from 'axios';
import * as moment from "moment";
// import Splash from './components/Splash';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:true
    }
  }


  render() {

    return (
      <Router>
        
        <div>
        
          <Route 
            exact path="/" 
            render={(props) => <Home loading={this.state.loading} testProps={this.state.testProps} {...props}  />}/>
          <Route 
            exact path="/dash"  
            render={(props) => <Dash 
                                  //heatmapData={this.state.newHeatMapDataPoints}
                                  //heatmapDataTest={this.state.heatmapDataTest}
                                  //newHeatMapDataPoints={this.state.newHeatMapDataPoints}
                                  //totalPoops={this.state.totalPoops}
                                  //windowPoops={this.state.windowPoopsTenSecs}
                                  //hourPoops={this.state.hourPoops}
                                  //showData={this.showData}
                                  //dayPoops={this.state.dayPoops}
                                  //hourlyPoops={this.state.hourlyPoops}
                                  {...props}  />}/>
          
          {/* <Route 
            exact path="/settings"
            component={Settings}/> */}
          <Route 
            exact path="/about" 
            component={About}/>
          <Route 
            exact path="/contact" 
            component={Contact}/>
          
        </div>
        
      </Router>

    );
  }
}

export default App;



/**
 * old
 * <div>
        <div>{this.state.loading ?  <Splash /> : <div></div>}</div>
        <div>
          YOU ARE NOW IN THE APP
        </div>
          <MainFlush />
          <AdvancedFlush />
      </div>



       //get heatmap data
    axios.get('https://hamburgesa-51dc3.firebaseio.com/heatmap.json')
       .then(response => {
            this.setState({heatmapData: response.data.filter(function (el) {
              return el != null;
            })})


    //get heatmap data straight from same point
    axios.get('https://hamburgesa-51dc3.firebaseio.com/pooper.json')
    .then(response => {
        // console.log(response)
        //this.setState({heatmapData: response.data})
        })
        
    //get heatmap from test points
    axios.get('https://hamburgesa-51dc3.firebaseio.com/testPoints.json')
    .then(response => {
         //console.log(response.data["-Lth7AL1rB1JfBi_Dm0A"])
        this.setState({heatmapDataTest: response.data["-LthEP_M5Zr2ztwUqno6"]})
        })

    //get heatmap data filtered for time 
    
            
    });




    --------------------------------------------------


      componentDidMount() {
    //mimic loading page
    setTimeout(() => {
      // console.log('loading page')
      this.setState({
        loading: false
      });
    }, 2500)

    setInterval(async () => {
    this.fetchData();  
    },10000)

   
        
  }

  setUserLocation = () => {
    // console.log("attempting to get user location ...")
    navigator.geolocation.getCurrentPosition(position => {

      let setUserLocation = [position.coords.latitude, position.coords.longitude];
      // console.log('user is at', setUserLocation);

     //set user state here
    });
  }

  fetchData = () => {
    const getWindow = async () => {
      await fetch('http://18.191.231.245:4500/stats')
        .then(res => res.json())
        .then(res => {

          // filter time stamps and lat lon correction, pull out only lat lon
          //latitude > 39 AND latitude< 45 
          //d['lon'] < -73 && d['lon'] > -70 &&
          //let resFilter = res.data.filter(d => {return (d['time'] >= this.state.windowStart || d['time'] < this.state.timestamp &&  d['lat'] > 0)}).map( o => { return { lat: o.lat, lon: o.lon, time: o.time} })
          //let resFilter = res.data.filter(d => {return (d['time'] >= this.state.windowStart || d['time'] < this.state.timestamp  ||  d['lat'] > 0)}).map( o => { return { lat: o.lat, lon: o.lon, time: o.time} })
          //resFilter=1;
          //console.log(res.data[0].poops)
          let resFilter = res.data.map( o => { return { lat: o.lat, lon: o.lon, time: o.time} })
          this.setState({ windowPoopsTenSecs: resFilter.length })
        }
        )
    }

       const getHour = async () => {
        let end = moment().format("YYYY-MM-DD HH:mm:00");
        let start = moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:00");

         await fetch(`http://18.191.231.245:4500/poops/${start}/${end}`)
           .then(res => res.json())
           .then(res => {
  
            //produce points for map
            let resFilter = res.data.map( o => { return { lat: o.lat, lon: o.lon, time: o.time} })

            //total poops in this hour window
            let hourPoops = resFilter.length.toLocaleString(resFilter.length, { minimumFractionDigits: 0 })
        
             this.setState({ hourPoops: hourPoops, newHeatMapDataPoints: resFilter })
           }
           )
       }

    

    const fetchSum = async () => {
      await fetch('http://18.191.231.245:4500/sum')
      .then(res => res.json())
      .then(res => {
        let allPoops=res.data[0].poops.toLocaleString(res.data[0].poops, { minimumFractionDigits: 0 })
        this.setState({ totalPoops: allPoops })
      }
      )
    }

    const fetchDailies = async () => {
      let end = moment().format("YYYY-MM-DD HH:mm:00");
      let start = moment().subtract(30, "days").format("YYYY-MM-DD HH:mm:00");
      await fetch(`http://18.191.231.245:4500/ppday/${start}/${end}`)
      .then(res => res.json())
      .then(res => {
        console.log(res.data)
        this.setState({ dayPoops: res.data})
      }
      )
    }

    const fetchHourlies = async () => {
      let end = moment().format("YYYY-MM-DD HH:mm:00");
      let start = moment().subtract(24, "hours").format("YYYY-MM-DD HH:mm:00");
      await fetch(`http://18.191.231.245:4500/pphour/${start}/${end}`)
      .then(res => res.json())
      .then(res => {
        //filter hour only hour and poops
        let resFilter = res.data.map( o => { return { hour: o.hour, poops: o.poops} })
        this.setState({ hourlyPoops: resFilter})
      }
      )
    }


    getWindow();
    getHour();
    fetchSum();
    fetchDailies();
    fetchHourlies();
  }




  ,
      location: null,
      totalPoops:null,
      windowPoops:null,
      hourPoops:null,
      windowPoopsTenSecs:null,
      newHeatMapDataPoints:[],
      dayPoops:[],
      hourlyPoops:[],
      timestamp: moment().format("YYYY-MM-DD HH:mm:00"),
      windowStart: moment().subtract(1, "hours").format("YYYY-MM-DD HH:mm:00"),
      windowStartDay: moment().subtract(24, "hours").format("YYYY-MM-DD HH:mm:00")



    */